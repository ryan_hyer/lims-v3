import { Meteor } from 'meteor/meteor';

import { Equipment } from '/imports/collections/equipment';

Meteor.startup(() => {
	Meteor.publish('equipment', function() {
		return Equipment.find({});
	})
});