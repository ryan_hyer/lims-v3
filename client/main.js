import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

//import App from '/imports/components/app';
import MainLayout from '/imports/components/main_layout';
import Dashboard from '/imports/components/dashboard';
import EquipmentList from '/imports/components/equipment/equipment_list';
import EquipmentDetail from '/imports/components/equipment/equipment_detail';

const routes = (
	<Router history={browserHistory}>
		<Route path="/" component={MainLayout}>
			<IndexRoute component={Dashboard} />
			<Route path="equipment" component={EquipmentList} />
		</Route>
	</Router>
);

// Render this component to the screen
Meteor.startup(() => {
  ReactDOM.render(routes, document.querySelector('.render-target'));
});