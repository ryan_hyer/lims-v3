import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

Meteor.methods({
	'equipment.insert': function(doc) {
		Equipment.insert(doc);
	}
});

export const Equipment = new Mongo.Collection('equipment');