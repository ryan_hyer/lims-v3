import React, { Component } from 'react';
import { Link } from 'react-router';
import AppBar from 'react-toolbox/lib/app_bar';

import Accounts from './accounts.js';

export default class HeaderAppBar extends Component {

	constructor(props) {
	  super(props);
	  this.state = {open: false};
	}

	handleToggle = () => this.setState({open: !this.state.open});
	handleClose = () => this.setState({open: false});

	render() {
		return(
			<div>
			  <AppBar>
			  	<Link to="/">OpenLIMS</Link>
			  </AppBar>
		  </div>
		);
	}
}