import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';

import { Equipment } from '/imports/collections/equipment';


class EquipmentList extends Component {
	constructor(props) {
		super(props);
	}
	
	renderList() {
		return this.props.equipment.map(eqItem => {
			return (
				<li key={eqItem._id}>
					{eqItem.identity}
				</li>
			);
		});
	}
	
	render () {
		console.log(this.props.equipment);
		return (
			<div>
				<h2>Equipment List</h2>
			  <ul>
			  	{this.renderList()}
			  </ul>
		  </div>
		);
	}
}

// createContainer makes React components reactive in Meteor
export default createContainer(() => {
	Meteor.subscribe('equipment');
	return { equipment: Equipment.find({}).fetch() };
}, EquipmentList);