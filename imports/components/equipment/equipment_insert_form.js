import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { FormsyCheckbox, FormsyDate, FormsyRadio, FormsyRadioGroup,
FormsySelect, FormsyText, FormsyTime, FormsyToggle } from 'formsy-material-ui/lib';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';

export default class EquipmentForm extends Component {
	constructor(props) {
    super(props);
    this.state = {
      canSubmit: false,
    };
  }

  enableButton = () => {
    this.setState({canSubmit: true});
  };

  disableButton = () => {
    this.setState({canSubmit: false});
  };

  submitForm = (data) => {
    //alert(JSON.stringify(data, null, 4));
    Meteor.call('equipment.insert',data);
  };

  notifyFormError = (data) => {
    console.error('Form error:', data);
  };

	render() {
	  let errorMessages= {
	    wordsError: "Please only use letters",
	    numericError: "Please provide a number",
	    urlError: "Please provide a valid URL",
	  };

	  let styles= {
	    paperStyle: {
	      //width: 300,
	      margin: 'auto',
	      padding: 20,
	    },
	    switchStyle: {
	      marginBottom: 16,
	    },
	    submitStyle: {
	      marginTop: 32,
	    },
	  };

		let { paperStyle, switchStyle, submitStyle } = styles;
		let { wordsError, numericError, urlError } = errorMessages;

		return (
      <Paper style={paperStyle}>
        <p>Items marked with * are required</p>
        <Formsy.Form
          onValid={this.enableButton}
          onInvalid={this.disableButton}
          onValidSubmit={this.submitForm}
          onInvalidSubmit={this.notifyFormError}
        >
          <FormsyText
            name="name"
            validations="isWords"
            validationError={wordsError}
            hintText="What is your name?"
            floatingLabelText="Name *"
            required
          />
          <Divider />
          <FormsyText
            name="age"
            validations="isNumeric"
            validationError={numericError}
            hintText="Are you old?"
            floatingLabelText="Age"
          /><br />
          <FormsyText
            name="url"
            validations="isUrl"
            validationError={urlError}
            required
            hintText="http://www.example.com"
            floatingLabelText="URL *"
          /><br />
          <FormsySelect
            name="frequency"
            required
            floatingLabelText="How often do you? *"
          >
            <MenuItem value={'never'} primaryText="Never" />
            <MenuItem value={'nightly'} primaryText="Every Night" />
            <MenuItem value={'weeknights'} primaryText="Weeknights" />
          </FormsySelect><br />
          <FormsyDate
            name="date"
            required
            floatingLabelText="Date *"
          /><br />
          <FormsyTime
            name="time"
            floatingLabelText="Time"
          /><br />
          <FormsyCheckbox
            name="agree"
            label="Do you agree to disagree?"
            style={switchStyle}
          /><br />
          <FormsyToggle
            name="toggle"
            label="Toggle"
            style={switchStyle}
          /><br />
          <FormsyRadioGroup name="shipSpeed" defaultSelected="not_light">
            <FormsyRadio
              value="light"
              label="prepare for light speed"
              style={switchStyle}
            />
            <FormsyRadio
              value="not_light"
              label="light speed too slow"
              style={switchStyle}
            />
            <FormsyRadio
              value="ludicrous"
              label="go to ludicrous speed"
              style={switchStyle}
              disabled={true}
            />
          </FormsyRadioGroup><br />
          <RaisedButton
            style={submitStyle}
            type="submit"
            label="Submit"
            disabled={!this.state.canSubmit}
          />
        </Formsy.Form>
			</Paper>
		);
	}
}
