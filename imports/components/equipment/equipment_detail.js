import React, { Component } from 'react';

const EquipmentDetail = (props) => {
	return (
    <li key={props.eqItem._id}>{props.eqItem.identity}</li>
	);
};

export default EquipmentDetail;