import React, { Component } from 'react';
import { Link } from 'react-router';
import Accounts from './accounts.js';

export default class MainLayout extends Component {
	constructor(props) {
		super(props);
  }

  render() {
    return (
    	<div className='containerMain'>
      	<Link to="/">OpenLIMS</Link><br />
      	<Link to="equipment">Equipment</Link>
      	<Accounts /> 
        {this.props.children}
      </div>
    );
  }
}