import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

export default class Accounts extends Component {
  componentDidMount() {
    // Render the Blaze accounts form to the component div
    this.view = Blaze.render(Template.loginButtons, ReactDOM.findDOMNode(this.refs.container));
  }
    
  componentWillUnmount() {
    // Destroy the Blaze form
    Blaze.remove(this.view);
  }
    
  render () {
    return(
      <div ref="container" id="accounts-container"></div>
    );
  }
}