import React from 'react';

import HeaderAppBar from './header_appbar';

export default (props) => {
  return (
	  <div>
  		<HeaderAppBar />
  		{props.children}
  	</div>
  );
};
